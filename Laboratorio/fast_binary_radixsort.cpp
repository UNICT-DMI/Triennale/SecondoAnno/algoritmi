/*CODE BY BLOODMAKER*/

#include <iostream>
#include <cstdlib>

using namespace std;

void CountingSort(unsigned int* A, unsigned int N, unsigned short pos);
unsigned int findMax(unsigned int* A, unsigned int N);
void RadixSort(unsigned int* A, unsigned int N);
inline unsigned short Log_2(unsigned int v);
inline unsigned short Digit(unsigned int v, unsigned short pos);

const unsigned int b[] = {0x2, 0xC, 0xF0, 0xFF00, 0xFFFF0000};
const unsigned short S[] = {1, 2, 4, 8, 16};

int main(void)
{
    return 0;
}

void CountingSort(unsigned int* A, unsigned int N, unsigned short pos)
{
    unsigned int C[2] = {0};
    for(unsigned int i = 0; i < N; ++i) C[Digit(A[i], pos)]++;
    C[1] += C[0];
    unsigned int output[N];
    for(int i = N - 1; i >= 0; --i) output[--C[Digit(A[i], pos)]] = A[i];
    for(unsigned int i = 0; i < N; ++i) A[i] = output[i];
}

void RadixSort(unsigned int* A, unsigned int N)
{
    unsigned int MAX = findMax(A, N);
    for(unsigned short pos = 0; pos <= Log_2(MAX); pos++) CountingSort(A, N, pos);
}

unsigned int findMax(unsigned int* A, unsigned int N)
{
    unsigned int MAX = A[0];
    for(unsigned int i = 0; i < N; ++i)
        MAX = A[i] ^ ((A[i] ^ MAX) & -(A[i] < MAX));
    return MAX;
}

inline unsigned short Log_2(unsigned int v)
{
    register unsigned short r = 0;
    for (short i = 4; i >= 0; i--)
    {
        if (v & b[i])
        {
            v >>= S[i];
            r |= S[i];
        }
    }
    return r;
}

inline unsigned short Digit(unsigned int v, unsigned short pos)
{
    return (v >> pos) & 0x01;
}