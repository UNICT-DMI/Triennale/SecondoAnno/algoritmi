# Algoritmi

## Slide

Le slide della corso A-L sono disponibili sul sito del Prof. Cantone, che provvederà a caricarle man mano che i vari argomenti verranno trattati a lezione. Sono disponibilie anche le slide degli anni passati.

È possibile inoltre consultare [CLRS](https://walkccc.github.io/CLRS/).